<?php

/**
 * Implements hook_rules_data_info().
 */
function cache_form_ttl_rules_data_info() {
  return array(
    'cache_form_ttl' => array(
      'label' => t('cache_form TTL'),
      'group' => t('cache_form TTL'),
      'wrap' => TRUE,
      'property info' => array(
        'form_id' => array(
          'label' => t("Form ID"),
          'type' => 'text',
          'description' => t("The form ID."),
        ),
        'expire' => array(
          'label' => t("cache_form TTL"),
          'type' => 'integer',
          'description' => t("The form ID."),
          'setter callback' => 'entity_property_verbatim_set',
        )
      ),
    ),
  );
}

/**
 * Implements hook_rules_event_info().
 */
function cache_form_ttl_rules_event_info() {
  $items = array(
    'cache_form_ttl_cache_set' => array(
      'label' => t('An element in cache_form is being set'),
      'group' => t('cache_form TTL'),
      'variables' => array(
        'cache_form_ttl_object' => array(
          'type' => 'cache_form_ttl',
          'label' => t('cache_form TTL object'),
        ),
      ),
    ),
  );

  return $items;
}

