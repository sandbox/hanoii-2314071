<?php

/**
 * @file
 * Default rules configurations.
 */

/**
 * Implements hook_default_rules_configuration().
 */
function cache_form_ttl_default_rules_configuration() {
  $configs = array();

  $rule = rules_reaction_rule();
  $rule->label = t('Update cache_form TTL');
  $rule->active = FALSE;
  $rule->event('cache_form_ttl_cache_set')
    ->condition('text_matches', array(
      'text:select' => 'cache_form_ttl_object:form-id',
      'match' => '.*',
      'operation' => 'regex'
    ))
    ->action('data_set', array(
      'data:select' => 'cache_form_ttl_object:expire',
      'value' => '21600',
    ));

  $configs['cache_form_ttl'] = $rule;

  return $configs;
}
